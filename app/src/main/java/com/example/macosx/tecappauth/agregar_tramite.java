package com.example.macosx.tecappauth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.macosx.tecappauth.Objetos.AdapterTramite;
import com.example.macosx.tecappauth.Objetos.Tramite;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class agregar_tramite extends AppCompatActivity {

    RecyclerView rvTramites;
    List<Tramite> tramites;
    AdapterTramite adapterTramite;
    DatabaseReference databaseReference;
    ImageButton btn_atras_tram;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_tramite);

        progressBar = (ProgressBar) findViewById(R.id.progressListaTramites);


        btn_atras_tram = (ImageButton) findViewById(R.id.btn_atras_tram);
        btn_atras_tram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent HomeAct = new Intent(getApplicationContext(), RecyclerActivity.class);
                startActivity(HomeAct);
            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference("tramite");
        rvTramites = (RecyclerView) findViewById(R.id.recyclerListaTramites);
        rvTramites.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        tramites = new ArrayList<>();
        adapterTramite = new AdapterTramite(tramites,getBaseContext());
        rvTramites.setAdapter(adapterTramite);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.VISIBLE);
                tramites.removeAll(tramites);
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Tramite tramite = snapshot.getValue(Tramite.class);
                    tramite.setId(snapshot.getKey());
                    tramites.add(tramite);
                }
                adapterTramite.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
