package com.example.macosx.tecappauth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.macosx.tecappauth.Objetos.Alumno;
import com.example.macosx.tecappauth.Objetos.Procedimientos;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Token;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;


public class PagoActivity extends AppCompatActivity implements Token.CreateToken{

    private static final String TAG = "PagoActivity";
    Double precio;
    String nombreTramite;
    String id_token;
    String nombreTipoTramite;

    ImageButton botonAtras;
    Button pagar;

    EditText editNombrePago;
    EditText editTarjetaPago;
    EditText editMesPago;
    EditText editAnioPago;
    EditText editCodigoPago;

    ProgressBar progressBar;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago );

        progressBar = (ProgressBar)findViewById(R.id.progressBarPago);

        editNombrePago = (EditText)findViewById(R.id.editNombrePago);
        editTarjetaPago = (EditText)findViewById(R.id.editTarjetaPAgo);
        editMesPago = (EditText)findViewById(R.id.editMesPago);
        editAnioPago = (EditText)findViewById(R.id.editAnioPago);
        editCodigoPago = (EditText)findViewById(R.id.codigoSeguridadPAgo);
        pagar = (Button)findViewById(R.id.botonPagarCarrito);



        botonAtras = (ImageButton) findViewById(R.id.btn_atras_pago);
        botonAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SelecTram = new Intent(getApplicationContext(), agregar_tramite.class);
                startActivity(SelecTram);
            }
        });
        getIncomingIntent();

        pagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editNombrePago.getText().toString().equals("") || editTarjetaPago.getText().toString().equals("") || editMesPago.getText().toString().equals("") || editAnioPago.getText().toString().equals("") || editCodigoPago.getText().toString().equals("")){
                    Toast.makeText(getBaseContext(), "Espacios vacíos, favor de llenar todos", Toast.LENGTH_SHORT).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                //preparaTarjeta();
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                Procedimientos p = new Procedimientos(databaseReference);
                String correo = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                //ArrayList<Alumno> lista = p.consultaAlumnoPorCorreo(correo);
                p.agregarSolicitud(correo,nombreTramite,nombreTipoTramite,precio);
                progressBar.setVisibility(View.GONE);
                Intent SelecTram = new Intent(getApplicationContext(), cuenta_creada.class);
                startActivity(SelecTram);
            }
        });
    }

    private void getIncomingIntent(){
        if(getIntent().hasExtra("totalTramite") && getIntent().hasExtra("nombreTramite") && getIntent().hasExtra("nombreTipoTramite")){
            precio = getIntent().getDoubleExtra("totalTramite",0);
            nombreTramite = getIntent().getStringExtra("nombreTramite");
            nombreTipoTramite = getIntent().getStringExtra("nombreTipoTramite");
        }
    }

    private void preparaTarjeta(){
        //Preparando conekta para pagos por internet :D
        Conekta.setPublicKey("key_PTsAmmzK7EbGzaf6em8DSPQ");
        Conekta.collectDevice(this);

        Card card = new Card(editNombrePago.getText().toString(),editTarjetaPago.getText().toString(),editCodigoPago.getText().toString(),
                editMesPago.getText().toString(),editAnioPago.getText().toString());

        Token token = new Token(this);
        token.onCreateTokenListener(this);
        token.create(card);
    }


    @Override
    public void onCreateTokenReady(JSONObject data) {
        try{
            //Toast.makeText(this,"Token creado: " + data.getString("id"),Toast.LENGTH_SHORT).show();
            id_token = data.getString("id");
        }catch (Exception e){

        }
    }
}
