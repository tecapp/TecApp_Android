package com.example.macosx.tecappauth.Objetos;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.macosx.tecappauth.R;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.SolicitudesViewHolder>{


    List<Solicitud> solicitudes;

    public Adapter(List<Solicitud> solicitudes) {
        this.solicitudes = solicitudes;
    }

    @NonNull
    @Override
    public SolicitudesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycler,parent, false);
        SolicitudesViewHolder holder = new SolicitudesViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SolicitudesViewHolder holder, int position) {
        Solicitud solicitud = solicitudes.get(position);
        holder.nombreTramiteHome.setText(solicitud.getNombreTramite());
        holder.estatusSolicitud.setText(solicitud.getEstatusAlumno());
        holder.nombreTipoTramiteHome.setText(solicitud.getNombreTipoTramite());
    }

    @Override
    public int getItemCount() {
        return solicitudes.size();
    }

    public static class SolicitudesViewHolder extends RecyclerView.ViewHolder{

        TextView estatusSolicitud, nombreTramiteHome, nombreTipoTramiteHome;

        public SolicitudesViewHolder(View itemView) {
            super(itemView);
            nombreTramiteHome = (TextView) itemView.findViewById(R.id.nombreTramiteHome);
            estatusSolicitud = (TextView) itemView.findViewById(R.id.estatusSolicitud);
            nombreTipoTramiteHome = (TextView) itemView.findViewById(R.id.nombreTipoTramiteHome);
        }
    }
}
