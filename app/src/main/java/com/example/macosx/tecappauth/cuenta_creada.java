package com.example.macosx.tecappauth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class cuenta_creada extends AppCompatActivity {


    Button btn_aceptar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_creada);

        btn_aceptar = (Button) findViewById(R.id.btn_aceptar_Listo);
        btn_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent MainAct = new Intent(getApplicationContext(), RecyclerActivity.class);
                startActivity(MainAct);
            }
        });
    }
}
