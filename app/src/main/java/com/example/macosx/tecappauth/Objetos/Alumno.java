package com.example.macosx.tecappauth.Objetos;

public class Alumno {
    private String num_control;
    private String nombre_alumno;
    private String primer_apellido;
    private String segundo_apellido;
    private String correo_electronico;
    private String id;

    public Alumno() {

    }

    public Alumno(String num_control, String nombre_alumno, String primer_apellido, String segundo_apellido, String correo_electronico) {
        this.num_control = num_control;
        this.nombre_alumno = nombre_alumno;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.correo_electronico = correo_electronico;
    }

    public String getNum_control() {
        return num_control;
    }

    public String getNombre_alumno() {
        return nombre_alumno;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setNum_control(String num_control) {
        this.num_control = num_control;
    }

    public void setNombre_alumno(String nombre_alumno) {
        this.nombre_alumno = nombre_alumno;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
