package com.example.macosx.tecappauth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.macosx.tecappauth.Objetos.Alumno;
import com.example.macosx.tecappauth.Objetos.Coche;
import com.example.macosx.tecappauth.Objetos.FirebaseReferences;
import com.example.macosx.tecappauth.Objetos.Procedimientos;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class registro_alumno extends AppCompatActivity {


    ImageButton btn_atras_reg;
    Button btn_registrarse;

    EditText nombre;
    EditText primer_apellido;
    EditText segundo_apellido;
    EditText correo_electronico;
    EditText contrasena;
    EditText numero_control;

    ProgressBar progressBar;

    Snackbar snackbar;
    boolean correcto;
    Alumno alumno;

    FirebaseDatabase database = FirebaseDatabase.getInstance();

    final DatabaseReference mDatabaseReference = database.getInstance().getReference(FirebaseReferences.ALUMNO_REFERENCE);

    FirebaseAuth.AuthStateListener mAuthListener;

    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_alumno);

        view = findViewById(R.id.activity_registro_alumno);
        //EditTexts
        nombre = (EditText) findViewById(R.id.editTextNombre);
        primer_apellido = (EditText) findViewById(R.id.editTextPrimerApellido);
        segundo_apellido = (EditText) findViewById(R.id.editTextSegundoApellido);
        correo_electronico = (EditText) findViewById(R.id.editTextCorreo);
        contrasena = (EditText) findViewById(R.id.editTextContra);
        numero_control = (EditText) findViewById(R.id.editTextNumeroControl);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_registro);

        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser(); //FirebaseAuth.getInstance().getCurrentUser();
                if(!(user != null)){
                    return;
                }
                //progressBar.setVisibility(View.GONE);
                //user.sendEmailVerification();
                //Intent CuentaCAct = new Intent(getApplicationContext(), cuenta_creada.class);
                //startActivity(CuentaCAct);
                //snackbar = Snackbar.make(view, user.getEmail().toString(), Snackbar.LENGTH_LONG);
                //snackbar.show();
            }
        };

        btn_atras_reg = (ImageButton) findViewById(R.id.btn_atras_reg);
        btn_atras_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent MainAct = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(MainAct);
            }
        });
        btn_registrarse = (Button) findViewById(R.id.btn_registrarse);
        btn_registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                progressBar.setVisibility(View.VISIBLE);
                String nombre_usuario = nombre.getText().toString();
                String pri_ape = primer_apellido.getText().toString();
                String seg_ape = segundo_apellido.getText().toString();
                String email = correo_electronico.getText().toString();
                String contra = contrasena.getText().toString();
                String nu_cont = numero_control.getText().toString();
                if(nombre_usuario.equals("") || pri_ape.equals("") || seg_ape.equals("") || email.equals("") || contra.equals("") || nu_cont.equals("")){
                    progressBar.setVisibility(View.GONE);
                    snackbar = Snackbar.make(view, "Hay campos vacíos, favor de llenarlos", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return;
                }
                alumno = new Alumno(nu_cont,nombre_usuario,pri_ape,seg_ape,email);
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, contra).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        View view = findViewById(R.id.activity_registro_alumno);
                        if(!task.isSuccessful()){
                            progressBar.setVisibility(View.GONE);
                            snackbar = Snackbar.make(view, task.getException().getMessage().toString(), Snackbar.LENGTH_LONG);
                            snackbar.show();
                            correcto = false;
                            return;
                        }
                        correcto = true;
                        //mDatabaseReference.child()
                        /*Procedimientos p = new Procedimientos();
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        if(p.eliminarUsuarioPorNoControl(mDatabaseReference, alumno, user, view)){
                            snackbar = Snackbar.make(view, "El numero de control ya esta en uso", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            correcto = false;
                            return;
                        }*/
                        mDatabaseReference.push().setValue(alumno);
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        user.sendEmailVerification();
                        progressBar.setVisibility(View.GONE);
                        snackbar = Snackbar.make(view, "Usuario creado correctamente, favor de verificar su correo", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                });
            }
        });


    }
}
