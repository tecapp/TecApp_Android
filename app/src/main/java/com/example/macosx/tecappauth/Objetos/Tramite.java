package com.example.macosx.tecappauth.Objetos;

public class Tramite {
    String nombreTramite;
    String tipoTramite;
    double precioTramite;
    String imagen;
    String id;
    String descripcion;

    public Tramite() {

    }

    public Tramite(String nombreTramite, String tipoTramite, double precioTramite, String imagen) {

        this.nombreTramite = nombreTramite;
        this.tipoTramite = tipoTramite;
        this.precioTramite = precioTramite;
        this.imagen = imagen;

    }

    public String getNombreTramite() {
        return nombreTramite;
    }

    public void setNombreTramite(String nombreTramite) {
        this.nombreTramite = nombreTramite;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public double getPrecioTramite() {
        return precioTramite;
    }

    public void setPrecioTramite(double precioTramite) {
        this.precioTramite = precioTramite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getImagen() {
        return imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
