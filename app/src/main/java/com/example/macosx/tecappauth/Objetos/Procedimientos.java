package com.example.macosx.tecappauth.Objetos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.example.macosx.tecappauth.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Procedimientos {

    //Numero de control encontrado
    private  DatabaseReference databaseReference;
    private boolean band = false;

    public Procedimientos(DatabaseReference databaseReference) {
        this.databaseReference = databaseReference;
    }

    public DatabaseReference getDatabaseReference() {
        return databaseReference;
    }

    public void setDatabaseReference(DatabaseReference databaseReference) {
        this.databaseReference = databaseReference;
    }

    public void agregarTramite(){

    }

    public Bitmap get_imagen(String url) {
        Bitmap bm = null;
        try {
            URL _url = new URL(url);
            URLConnection con = _url.openConnection();
            con.connect();
            InputStream is = con.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (IOException e) {

        }
        return bm;
    }

    public boolean agregarSolicitud(String correo, String nombreTramite, String nombreTipoTramite,Double precio){
        //String id_alumno = this.id_alumno;
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        Date date = c.getTime();
        String estatus = "En proceso";
        Solicitud solicitud = new Solicitud(correo,nombreTramite,nombreTipoTramite,date.toString(),estatus,precio);
        databaseReference.child("solicitud").push().setValue(solicitud);
        band = true;
        return band;
    }

    public void getInformacionAlumno(String correo){
        final List<Alumno> lista = new ArrayList<>();
        Query consulta = databaseReference.child("alumno").orderByChild("correo_electronico").equalTo(correo);
        consulta.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Alumno alumno = snapshot.getValue(Alumno.class);
                    alumno.setId(snapshot.getKey());
                    lista.add(alumno);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
