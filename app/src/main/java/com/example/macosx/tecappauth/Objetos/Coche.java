package com.example.macosx.tecappauth.Objetos;

public class Coche {
    String dueno;
    String marca;
    int puertas;
    int ruedas;

    public Coche() {
    }

    public Coche(String dueno, String marca, int puertas, int ruedas) {
        this.dueno = dueno;
        this.marca = marca;
        this.puertas = puertas;
        this.ruedas = ruedas;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public int getRuedas() {
        return ruedas;
    }

    public void setRuedas(int ruedas) {
        this.ruedas = ruedas;
    }
}
