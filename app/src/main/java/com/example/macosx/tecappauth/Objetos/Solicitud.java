package com.example.macosx.tecappauth.Objetos;

import java.util.Date;

public class Solicitud {
    private String idSolicitud;
    private String correoAlumno;
    private String nombreTramite;
    private String nombreTipoTramite;
    private String fechaSolicitud;
    private String estatusAlumno;
    private Double precio;

    public Solicitud() {
    }

    public Solicitud(String correoAlumno, String nombreTramite, String nombreTipoTramite, String fechaSolicitud, String estatusAlumno, Double precio) {
        this.correoAlumno = correoAlumno;
        this.nombreTramite = nombreTramite;
        this.nombreTipoTramite = nombreTipoTramite;
        this.fechaSolicitud = fechaSolicitud;
        this.estatusAlumno = estatusAlumno;
        this.precio = precio;
    }

    public String getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getCorreoAlumno() {
        return correoAlumno;
    }

    public void setCorreoAlumno(String correoAlumno) {
        this.correoAlumno = correoAlumno;
    }

    public String getNombreTramite() {
        return nombreTramite;
    }

    public void setNombreTramite(String nombreTramite) {
        this.nombreTramite = nombreTramite;
    }

    public String getNombreTipoTramite() {
        return nombreTipoTramite;
    }

    public void setNombreTipoTramite(String nombreTipoTramite) {
        this.nombreTipoTramite = nombreTipoTramite;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getEstatusAlumno() {
        return estatusAlumno;
    }

    public void setEstatusAlumno(String estatusAlumno) {
        this.estatusAlumno = estatusAlumno;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
