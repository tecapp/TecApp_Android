package com.example.macosx.tecappauth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.macosx.tecappauth.Objetos.FirebaseReferences;
import com.example.macosx.tecappauth.Objetos.Adapter;
import com.example.macosx.tecappauth.Objetos.Coche;
import com.example.macosx.tecappauth.Objetos.MyRandom;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    private BottomNavigationView mMainNav;
    private FloatingActionButton mainFAB;
    private FrameLayout mMainFrame;

    private HomeFragment home_fragment;
    private ConfigurationFragment conf_fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        mMainNav = (BottomNavigationView)findViewById(R.id.main_nav);
        mainFAB = (FloatingActionButton)findViewById(R.id.mainFAB);
        mMainFrame = (FrameLayout)findViewById(R.id.main_frame);

        home_fragment = new HomeFragment();
        conf_fragment = new ConfigurationFragment();

        //Aplicar un Fragment
        setFragment(home_fragment);
        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.inicio_item:
                        setFragment(home_fragment);
                        break;

                    case R.id.conf_item:
                        setFragment(conf_fragment);
                        break;
                }
                return false;
            }
        });

        //Floating botton
        mainFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent AgTramite = new Intent(getApplicationContext(), agregar_tramite.class);
                startActivity(AgTramite);
            }
        });

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorLetra));
        toolbar.setDrawingCacheBackgroundColor(getResources().getColor(R.color.colorLetra));
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame,fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.dash_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.logout_item){
            FirebaseAuth.getInstance().signOut();
            Intent MainAct = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(MainAct);
        }
        return true;
    }
}
