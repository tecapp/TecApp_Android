package com.example.macosx.tecappauth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import android.support.design.widget.Snackbar;
import android.support.design.widget.CoordinatorLayout;
import android.widget.ProgressBar;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnCompleteListener {

    EditText email, pass;
    Button iniBtn, regBtn;

    String em,pa;

    private CoordinatorLayout coordinatorLayout;

    FirebaseAuth.AuthStateListener mAuthListener;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = (EditText) findViewById(R.id.emailText);
        pass = (EditText) findViewById(R.id.passwordText);
        iniBtn = (Button) findViewById(R.id.inicButton);
        regBtn = (Button) findViewById(R.id.regButton);

        iniBtn.setOnClickListener(this);
        regBtn.setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBarLogin);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser(); //FirebaseAuth.getInstance().getCurrentUser();
                if(!(user != null)){
                    return;
                }
                if(!user.isEmailVerified()){
                    //Snackbar snackbar = Snackbar.make(coordinatorLayout, "Correo electrónico no verificado favor de verificarlo", Snackbar.LENGTH_LONG);
                    //snackbar.show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }
                progressBar.setVisibility(View.GONE);
                Intent RecAct = new Intent(getApplicationContext(), RecyclerActivity.class);
                startActivity(RecAct);
            }
        };
    }

    private void iniciarSesion(String email, String pass){
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, pass).addOnCompleteListener(this);

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.inicButton:
                if(email.getText().toString().equals("") || pass.getText().toString().equals("")){
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, R.string.mensaje_login_vacios, Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                em = email.getText().toString();
                pa = pass.getText().toString();
                iniciarSesion(em,pa);
                break;
            case R.id.regButton:
                //em = email.getText().toString();
                //pa = pass.getText().toString();
                //registrar(em,pa);
                Intent RegAct = new Intent(getApplicationContext(), registro_alumno.class);
                startActivity(RegAct);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthListener != null){
            FirebaseAuth.getInstance().removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onComplete(@NonNull Task task) {
        if(!(task.isSuccessful())){
            Snackbar snackbar = Snackbar.make(coordinatorLayout, task.getException().getMessage().toString(), Snackbar.LENGTH_LONG);
            snackbar.show();
            progressBar.setVisibility(View.GONE);
            return;
        }
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(!user.isEmailVerified()){
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Correo electrónico no verificado favor de verificarlo", Snackbar.LENGTH_LONG);
            snackbar.show();
            progressBar.setVisibility(View.GONE);
            return;
        }
        Intent RecAct = new Intent(getApplicationContext(), RecyclerActivity.class);
        startActivity(RecAct);
    }
}
