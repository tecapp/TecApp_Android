package com.example.macosx.tecappauth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DecimalFormat;

public class carrito extends AppCompatActivity {


    //En un futuro debera ser un recycler view para que sea un carrito con lista de lo que vas insertando
    private TextView tipoTramite;
    private TextView descripcionTramiteCarrito;
    private TextView pagoTramiteCarrito;
    private ImageButton atrasCarrito;
    private Button siguienteCarrito;

    String precio;
    String nomT;
    String tipoNomT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);


        atrasCarrito = (ImageButton) findViewById(R.id.btn_atras_carrito);
        atrasCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SelecTram = new Intent(getApplicationContext(), agregar_tramite.class);
                startActivity(SelecTram);
            }
        });
        tipoTramite = (TextView) findViewById(R.id.nombreTramiteCarrito);
        descripcionTramiteCarrito = (TextView) findViewById(R.id.descripcionTramiteCarrito);
        pagoTramiteCarrito = (TextView) findViewById(R.id.textTotalCarrito);

        getIncomingIntent();

        siguienteCarrito = (Button) findViewById(R.id.botonSiguienteCarrito);
        siguienteCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pagoAct = new Intent(getApplicationContext(), PagoActivity.class);
                pagoAct.putExtra("totalTramite", Double.parseDouble(precio));
                pagoAct.putExtra("nombreTramite",nomT);
                pagoAct.putExtra("nombreTipoTramite",tipoNomT);
                startActivity(pagoAct);
            }
        });
    }

    private void getIncomingIntent(){
        if (getIntent().hasExtra("id_tramite") && getIntent().hasExtra("nombre_tramite") && getIntent().hasExtra("nombre_tipo_tramite") && getIntent().hasExtra("precio_tramite") && getIntent().hasExtra("descripcion_tramite")){
            String idTramite = getIntent().getStringExtra("id_tramite");
            String nombreTramite = getIntent().getStringExtra("nombre_tramite");
            String nombreTipoTramite = getIntent().getStringExtra("nombre_tipo_tramite");
            String precioTramite = getIntent().getStringExtra( "precio_tramite");
            String descripcionTramite = getIntent().getStringExtra("descripcion_tramite");

            tipoTramite.setText(nombreTramite + " de " + nombreTipoTramite);
            descripcionTramiteCarrito.setText(descripcionTramite);
            pagoTramiteCarrito.setText("$"+precioTramite);
            precio = precioTramite;
            nomT = nombreTramite;
            tipoNomT = nombreTipoTramite;
        }
    }
}
