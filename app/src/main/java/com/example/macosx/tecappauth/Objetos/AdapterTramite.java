package com.example.macosx.tecappauth.Objetos;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.macosx.tecappauth.PagoActivity;
import com.example.macosx.tecappauth.R;
import com.example.macosx.tecappauth.carrito;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterTramite extends RecyclerView.Adapter<AdapterTramite.TramitesViewHolder>{


    private List<Tramite> tramites;
    private Context context;

    public AdapterTramite(List<Tramite> tramites, Context context) {
        this.tramites = tramites;
        this.context = context;
    }

    @NonNull
    @Override
    public TramitesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycler_tramite,parent, false);
        TramitesViewHolder holder = new TramitesViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TramitesViewHolder holder, final int position) {
        Procedimientos p = new Procedimientos(FirebaseDatabase.getInstance().getReference());
        final Tramite tramite = tramites.get(position);
        holder.textViewNombreTramite.setText(tramite.getNombreTramite());
        holder.textviewTipoTramite.setText(tramite.getTipoTramite());
        holder.textviewPrecioTramite.setText(tramite.getPrecioTramite()+"" );
        holder.imagenTramite.setImageBitmap(p.get_imagen(tramite.getImagen()));
        //Glide.with(context).asBitmap().load(tramite.getURL()).into(holder.imagenTramite);
        holder.elementoTramite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Lista",tramites.get(position).getTipoTramite());
                //Toast.makeText(context, tramites.get(position).getTipoTramite().toString(),Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(context, carrito.class);
                intent.putExtra("id_tramite",tramite.getId());
                intent.putExtra("nombre_tramite", tramite.getNombreTramite());
                intent.putExtra("nombre_tipo_tramite", tramite.getTipoTramite());
                intent.putExtra("precio_tramite", tramite.getPrecioTramite()+"");
                intent.putExtra("descripcion_tramite", tramite.getDescripcion());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tramites.size();
    }

    public static class TramitesViewHolder extends RecyclerView.ViewHolder{

        TextView textViewNombreTramite, textviewTipoTramite, textviewPrecioTramite;
        CircleImageView imagenTramite;
        RelativeLayout elementoTramite;

        public TramitesViewHolder(View itemView) {
            super(itemView);
            imagenTramite = (CircleImageView) itemView.findViewById(R.id.imagenTramite);
            textViewNombreTramite = (TextView) itemView.findViewById(R.id.textviewNombreTramite);
            textviewTipoTramite = (TextView) itemView.findViewById(R.id.textviewTipoTramite);
            textviewPrecioTramite = (TextView) itemView.findViewById(R.id.textviewPrecioTramite);
            elementoTramite = (RelativeLayout) itemView.findViewById(R.id.elementoTramite);
        }
    }
}
