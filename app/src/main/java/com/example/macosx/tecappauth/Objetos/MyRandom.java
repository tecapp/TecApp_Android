package com.example.macosx.tecappauth.Objetos;

import java.util.Random;

public class MyRandom {
    static Random R=new Random();
    public static int nextInt(int Num){
        return R.nextInt(Num);
    }
    public static int nextInt(int LimInf,int LimSup){
        return R.nextInt(  LimSup-LimInf+1)+LimInf;
    }


    static String [] Nom={"Alicia","Sofia","Juan","Arturo","Eliseo","Federico","Natalia"};
    static String [] Ap={"Lopez","Perez","Garcia","Hernandez","Rodriguez","Peraza","Nieto"};
    static String [] CarMarc={"Ford","Nissan","Dodge","Kia","Chevrolet"};

    public static String nextName(){
        return Nom[nextInt(Nom.length)]+" "+Ap[ nextInt(Ap.length)]+" "+Ap[nextInt(Ap.length)];

    }

    public static String nextCar(){
        return CarMarc[nextInt(CarMarc.length)];
    }
}
