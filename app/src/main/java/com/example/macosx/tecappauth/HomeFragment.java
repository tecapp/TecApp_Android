package com.example.macosx.tecappauth;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.macosx.tecappauth.Objetos.Adapter;
import com.example.macosx.tecappauth.Objetos.Coche;
import com.example.macosx.tecappauth.Objetos.FirebaseReferences;
import com.example.macosx.tecappauth.Objetos.MyRandom;
import com.example.macosx.tecappauth.Objetos.Solicitud;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    RecyclerView rv;

    List<Solicitud> solicitudes;

    Adapter adapter;

    DatabaseReference mDatabaseReference;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mDatabaseReference = database.getReference("solicitud");
        rv = (RecyclerView) v.findViewById(R.id.recycler);
        rv.setLayoutManager(new LinearLayoutManager(v.getContext()));
        solicitudes = new ArrayList<>();
        adapter = new Adapter(solicitudes);
        rv.setAdapter(adapter);
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                solicitudes.removeAll(solicitudes);
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Solicitud solicitud = snapshot.getValue(Solicitud.class);
                    if(solicitud.getCorreoAlumno().equals(FirebaseAuth.getInstance().getCurrentUser().getEmail())){
                        solicitudes.add(solicitud);
                    }
                    //solicitudes.add(solicitud);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return v;
    }


}
